from gettraffic import *
import iso8601

cur = con.cursor()
updatecur = con.cursor()

days = ['maandag',
        'dinsdag',
        'woensdag',
        'donderdag',
        'vrijdag',
        'zaterdag',
        'zondag']
for r in cur.execute("SELECT rowid, timestamp FROM traffic;"):
	rowid = r[0]
   	day = days[iso8601.parse_date(r[1]).weekday()]
   	# print rowid, day
   	updatecur.execute("UPDATE traffic SET day = ? WHERE rowid = ?", (day, rowid))
   	# print "updated!"

con.commit()
