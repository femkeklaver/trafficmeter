# get traffic info from vid
from datetime import datetime
from lxml import html
from elasticsearch import Elasticsearch

import sqlite3


def create_table():
    cur = con.cursor()
    cur.execute("""CREATE TABLE IF NOT EXISTS traffic
                (timestamp text,
                road text,
                traject text,
                knooppunt text,
                lengte int)""")
    con.commit()


def parse_page(url):
    es = Elasticsearch()
    tree = html.parse(url)

    dt = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ')

    dag = days[datetime.utcnow().weekday()]
    prev_weg = ""
    prev_traject = ""

    for file in tree.xpath('//div[@id="overzicht-verkeer"]/dl'):
        # print html.tostring(file)

        weg = file.xpath('./dt/span/text()')
        traject = file.xpath(
            './dt[@class="vi-hoofdtraject vi-bericht"]/text()')
        # print "prevweg = ", prev_weg
        if weg and weg[0].strip() != "":
            weg = weg[0]
            prev_weg = weg
            # print "Succes! weg = ", weg

        else:
            weg = prev_weg
            # print "Fail! weg = ", weg

        lengte = file.xpath('.//span[@class="vi-km"]/text()')
        if not lengte:
            continue
        lengte_str = lengte[0]

        # print "prevtraject = ", prev_traject
        if traject and traject[0].strip() != "":
            traject = traject[0]
            prev_traject = traject
            # print "Succes! Traject = ", traject
        else:
            traject = prev_traject
            # print "Fail! traject = ", traject

        knooppunt = file.xpath(
            './dd[@class="vi-traject vi-bericht"]/text()')[0]
        lengte = int(lengte_str.replace("km", "").strip())

        cur = con.cursor()
        cur.execute("INSERT INTO traffic VALUES (?, ?, ?, ?, ?, ?)",
                    (dt, weg, traject, knooppunt, lengte, dag))

        es.index(index="my-index", doc_type="file",
                 body={"timestamp": dt,
                       "road": weg,
                       "traject": traject,
                       "knooppunt": knooppunt,
                       "lengte": lengte})
    con.commit()

if __name__ == "__main__":
    con = sqlite3.connect("/Volumes/MyBook/vid/vid.db")
    days = ['maandag',
            'dinsdag',
            'woensdag',
            'donderdag',
            'vrijdag',
            'zaterdag',
            'zondag']
    create_table()
    prev_weg = ""
    prev_traject = ""
    parse_page("http://vid.nl/VI/overzicht")
