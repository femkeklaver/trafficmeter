import sqlite3
from urllib import unquote_plus
from collections import defaultdict

from flask import Flask, jsonify, render_template, g, request
app = Flask(__name__)

DATABASE = "../vid/vid.db"


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


@app.route('/')
def index():
    cur = get_db().cursor()
    traffic_info = []
    traffic_dict = defaultdict(list)
    time_dict = defaultdict(dict)
    # dict name: traject

    for r in cur.execute("""select DISTINCT time((strftime('%s', timestamp) / 300) * 300, 
                            'unixepoch', '+2 hours') t, traject, knooppunt, 
                            avg(lengte) FROM traffic WHERE 
                            strftime('%w', timestamp) = strftime('%w', 'now') 
                            AND road = 'A9'
                            AND traject != ''
                            GROUP BY t, traject;"""):
        # traffic_info.append({"time": r[0],
        #                      "traject": r[1],
        #                      "knooppunt": r[2],
        #                      "lengte": r[3]})
        traffic_dict[r[1]].append({"time": r[0],
                                   "lengte": r[3]})
        
        # time_dict[r[0]] = {}
        if r[1] in time_dict[r[0]]:
            time_dict[r[0][r[1]]]["lengte"] += r[3]
        else:
            time_dict[r[0]][r[1]] = {
                                "lengte": r[3]}

    # for k, v in traffic_dict.iteritems():
    #     traffic_info.append({"name": k, "data": v})
    trajects = traffic_dict.keys()
    for t in trajects:
        traject_dict = {}
        traject_dict["name"] = t
        traject_dict["data"] = []
        for k, v in time_dict.iteritems():
        
            traject_dict["data"].append({"time": k,
                                    "lengte": v[t]["lengte"] if t in v else 0})
        traffic_info.append(traject_dict)
            

        # {"name" traject: "data": {"time": k, "lengte": v["lengte"]}}
  
    return render_template('index.html', traffic_info=traffic_info)
    # roads = []
    # trajects = []
    # for r in cur.execute("select DISTINCT road, traject FROM traffic;"):
    #     roads.append(r[0])
    #     trajects.append(r[1])

    # days = [r[0] for r in cur.execute("select DISTINCT day FROM traffic")]
    # trajects = [r[0] for r in cur.execute("select DISTINCT traject FROM traffic")]
    # return render_template('index.html', roads=roads, days=days,
    # trajects=trajects)
@app.route('/test', methods=['POST', 'GET'])
def test():
    cur = get_db().cursor()

    traject = unquote_plus(request.args.get('test'))
    results = {"data": []}
    for r in cur.execute("""SELECT DISTINCT time((strftime('%s', timestamp) / 300) * 300, 
                            'unixepoch', '+2 hours') t, knooppunt, avg(lengte)
                            FROM traffic WHERE traject = ? AND 
                            strftime('%w', timestamp) = strftime('%w', 'now') 
                            GROUP BY t, knooppunt""", (traject,)):
        results["data"].append({"time": r[0],
                                "knooppunt": r[1],
                                "lengte": r[2]})
    return jsonify(results)


@app.route('/get_traject')
def get_traject():
    cur = get_db().cursor()
    road = request.args.get('road')
    print road
    trajects = cur.execute(
        "select DISTINCT traject FROM traffic WHERE road = ?", (road,))
    print trajects
    return render_template('index.html', roads=road, days=days, trajects=trajects)


@app.route('/get_knooppunt')
@app.route('/get_traffic', methods=['POST', 'GET'])
def get_traffic():
    cur = get_db().cursor()
    day = request.form['day']
    road = request.form['road']
    for r in cur.execute("""select DISTINCT time(timestamp), traject, knooppunt, 
                        avg(lengte) FROM traffic WHERE day = %s AND road = %s
                        GROUP BY t, traject;""", (day, road)):
        time = r[0]
        traject = r[1]
        knooppunt = r[2]
        lengte = r[3]

if __name__ == "__main__":
    # con = sqlite3.connect("/Volumes/MyBook/vid/vid.db")
    # cur = con.cursor()

    app.run(debug=True)
