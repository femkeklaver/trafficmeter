from gettraffic import *

body = {
        "mappings": {
            "file": {
                "properties": {
                    "knooppunt": {
                        "type": "string",
                        "index": "not_analyzed"
                    },
                    "lengte": {
                        "type": "long"
                    },
                    "road": {
                        "type": "string",
                        "index": "not_analyzed"
                    },
                    "timestamp": {
                        "type": "date",
                        "format": "strict_date_optional_time||epoch_millis"
                    },
                    "traject": {
                        "type": "string",
                        "index": "not_analyzed"
                    },
                    "day": {
                        "type": "string",
                        "index": "not_analyzed"
                    }
                }
            }
        }
        }


es.indices.create(index="traffic", body=body)

cur = con.cursor()
for dt, weg, traject, knooppunt, lengte, dag in cur.execute("select * from traffic"):
    es.index(index="traffic", doc_type="file",
         body={"timestamp": dt,
               "road": weg,
               "traject": traject,
               "knooppunt": knooppunt,
               "lengte": lengte,
               "day": dag})   
